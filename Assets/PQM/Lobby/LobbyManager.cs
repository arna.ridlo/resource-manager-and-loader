using UnityEngine;
using Arna.Integration;
using Arna.Loading;

public class LobbyManager : MonoBehaviour
{
#if UNITY_EDITOR
    [Header("Editor")]
    public bool debugLogin;
    public string debugUsername;
    public string debugPassword;
#endif

    // Start is called before the first frame update
    private void Start()
    {
#if UNITY_EDITOR
        if (debugLogin)
            LoadingManager.StartLoading("login", IntegrationManager.GetRequest("login", new IntegrationManager.AuthForm(debugUsername, debugPassword)));
#endif

        if (LoadingManager.StartLoading("getBooth", IntegrationManager.GetRequest("getBooth", query: "/1")))
        {
            var process = LoadingManager.GetLoading("getBooth");
            process.onFinished += () =>
            {
                Debug.Log(process.webRequest.downloadHandler.text);
            };
        }
    }
}
