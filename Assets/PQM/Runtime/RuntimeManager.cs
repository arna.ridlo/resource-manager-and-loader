using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Arna.Runtime
{
    using Integration;

    public class RuntimeManager : MonoBehaviour
    {
        private static RuntimeManager _instance;

        #region PUBLIC VARIABLES
        public string defaultScene;
        public Camera defaultCamera;
        public IntegrationProfile integrationProfile;
        #endregion

        #region STATIC VARIABLES
        public static bool IsReady => _instance != null;
        public static bool IsAuthorized => IntegrationManager.IsAuthorized;
        #endregion

        #region PRIVATE VARIABLES
        private static List<string> _activeScenes;
        #endregion

        #region CONSTANT VARIABLES
        private const string RUNTIME_SCENE = "Runtime";
        #endregion

        #region INITIALIZATION
        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            _activeScenes = new List<string>();
            if (_instance == null)
            {
                _activeScenes.Add(SceneManager.GetActiveScene().name);
                SceneManager.LoadSceneAsync(RUNTIME_SCENE, LoadSceneMode.Additive);
            }
            else
            {
                LoadScene(_instance.defaultScene, LoadSceneMode.Single);
            }
        }

        private void Awake()
        {
            _instance = this;
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
            IntegrationManager.Initialize(integrationProfile);
        }
        #endregion

        #region SCENE MANAGEMENT
        private void OnSceneLoaded(Scene scn, LoadSceneMode mode)
        {
            Debug.Log("Scene loaded: " + scn.name);
            defaultCamera.gameObject.SetActive(FindObjectsOfType(typeof(Camera), true).Length <= 1);
        }

        private void OnSceneUnloaded(Scene scn)
        {
            Debug.Log("Scene unloaded: " + scn.name);
            defaultCamera.gameObject.SetActive(FindObjectsOfType(typeof(Camera), true).Length <= 1);
        }

        public static void LoadScene(string sname, LoadSceneMode mode)
        {
            if (_activeScenes.Contains(sname))
                return;

            _instance.StartCoroutine(_instance.LoadingScene(sname, mode));
        }

        private IEnumerator LoadingScene(string sname, LoadSceneMode mode)
        {

            if (mode == LoadSceneMode.Single)
            {
                foreach (var e in _activeScenes)
                {
                    yield return SceneManager.UnloadSceneAsync(e);
                }
                _activeScenes.Clear();
            }
            yield return SceneManager.LoadSceneAsync(sname, LoadSceneMode.Additive);
            _activeScenes.Add(sname);
        }
        #endregion

        #region ASSET MANAGEMENT
        public static T LoadAsset<T>(string path)
        {
            Debug.Log("Loading asset of type " + typeof(T) + " at: " + path);
            T asset = default;
            return asset;
        }
        #endregion
    }
}