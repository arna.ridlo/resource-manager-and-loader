using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arna.Integration
{
    [CreateAssetMenu(fileName = "New IntegrationProfile", menuName = "Vixbit/Integration Profile")]
    public class IntegrationProfile : ScriptableObject
    {
        public string baseUrl;
        public IntegrationAPI[] api;
    }

    [System.Serializable]
    public struct IntegrationAPI
    {
        public string name;
        public string endPoint;
        public IntegrationMethod method;
    }

    public enum IntegrationMethod
    {
        Get = 0,
        Post = 1,
        Put = 2,
        Delete = 3
    }
}