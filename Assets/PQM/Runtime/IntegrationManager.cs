using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Arna.Integration
{
    using Loading;

    public class IntegrationManager
    {
        public static string AuthToken { get; private set; }
        public static UserData User { get; private set; }
        public static bool IsAuthorized => !string.IsNullOrEmpty(AuthToken);

        private static string _baseUrl;
        private static Dictionary<string, IntegrationAPI> _api;

        public static void Initialize(IntegrationProfile profile)
        {
            _baseUrl = profile.baseUrl;

            _api = new Dictionary<string, IntegrationAPI>();
            foreach(var e in profile.api)
            {
                _api.Add(e.name, e);
            }

            if (PlayerPrefs.HasKey("token"))
            {
                AuthToken = PlayerPrefs.GetString("token");
                LoadingExec.Run(FetchUserData());
            }
        }

        public static UnityWebRequest GetRequest(string key, object payload = null, string query = null)
        {
            if(!_api.ContainsKey(key))
            {
                Debug.LogWarning("API '" + key + "' doesn't exist in integration profile.");
                return null;
            }

            UnityWebRequest request = null;
            var api = _api[key];
            switch (api.method)
            {
                case IntegrationMethod.Get:
                    request = UnityWebRequest.Get(GetUrl(key));
                    break;
                case IntegrationMethod.Post:
                    request = UnityWebRequest.Put(GetUrl(key), ToJsonBytes(payload));
                    request.method = UnityWebRequest.kHttpVerbPOST;
                    SetContentTypeJson(request);
                    break;
                case IntegrationMethod.Put:
                    request = UnityWebRequest.Put(GetUrl(key), ToJsonBytes(payload));
                    SetContentTypeJson(request);
                    break;
                case IntegrationMethod.Delete:
                    request = UnityWebRequest.Delete(GetUrl(key));
                    break;
            }

            if (!string.IsNullOrEmpty(query))
                request.url += query;

            if(IsAuthorized)
                Authorize(request);

            if (key == "login" || key == "register")
                LoadingExec.Run(Authenticate(request));

            return request;
        }

        private static IEnumerator Authenticate(UnityWebRequest request)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitUntil(() => request.isDone);

            if (!IsRequestError(request))
            {
                var auth = JsonUtility.FromJson<AuthData>(request.downloadHandler.text);
                AuthToken = auth.jwt;
                User = auth.user;
                PlayerPrefs.SetString("token", AuthToken);
                Debug.Log("User authenticated as " + User.username + ", role: " + User.role.name);
            }
        }

        private static IEnumerator FetchUserData()
        {
            yield return new WaitUntil(() => IsAuthorized);
            if (LoadingManager.StartLoading("fetchUserMe", GetRequest("getUserMe")))
            {
                var fetchUser = LoadingManager.GetLoading("fetchUserMe");
                fetchUser.onFinished += () =>
                {
                    User = JsonUtility.FromJson<UserData>(fetchUser.webRequest.downloadHandler.text);
                    Debug.Log("User authenticated as " + User.username + ", role: " + User.role.name);
                };
            }
        }

        #region UTILITY
        public static byte[] ToJsonBytes(object target)
        {
            if (target == null)
                return null;
            return System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(target));
        }

        public static void SetContentTypeJson(UnityWebRequest request)
        {
            request.SetRequestHeader("content-type", "application/json; charset=utf-8");
        }

        public static void Authorize(UnityWebRequest request)
        {
            if (!IsAuthorized)
                return;
            request.SetRequestHeader("Authorization", "Bearer " + AuthToken);
        }

        private static string GetUrl(string key)
        {
            if (!_api.ContainsKey(key))
                return null;

            return _baseUrl + _api[key].endPoint;
        }

        private static bool IsRequestError(UnityWebRequest request)
        {
            return request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.DataProcessingError || request.result == UnityWebRequest.Result.ProtocolError;
        }
        #endregion

        #region AUTH DATA
        [System.Serializable]
        public struct AuthForm
        {
            public string identifier;
            public string password;

            public AuthForm(string identifier, string password)
            {
                this.identifier = identifier;
                this.password = password;
            }
        }        
        
        [System.Serializable]
        public struct AuthData
        {
            public string jwt;
            public UserData user;
        }

        [System.Serializable]
        public struct UserData
        {
            public int id;
            public string username;
            public string email;
            public AuthRole role; 
        }

        [System.Serializable]
        public struct AuthRole
        {
            public int id;
            public string name;
            public string description;
            public string type;
        }
        #endregion
    }
}