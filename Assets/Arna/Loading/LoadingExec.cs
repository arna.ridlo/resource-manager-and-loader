using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingExec : MonoBehaviour
{
    private static LoadingExec instance;

    private const string TARGET_SCENE = "Runtime";

    public static Coroutine Run(IEnumerator routine)
    {
        if (instance == null)
        {
            instance = new GameObject().AddComponent<LoadingExec>();
            instance.gameObject.name = "LoadingExec";
            if(string.IsNullOrEmpty(TARGET_SCENE))
                DontDestroyOnLoad(instance);
            else
                SceneManager.MoveGameObjectToScene(instance.gameObject, SceneManager.GetSceneByName(TARGET_SCENE));

            Debug.Log("Created loading exec");
        }
        return instance.StartCoroutine(routine);
    }
}
